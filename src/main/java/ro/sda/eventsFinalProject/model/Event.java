package ro.sda.eventsFinalProject.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(nullable = false)  // ca sa nu poti introduce in baza de date fara nume
    private String name;
    @Column(name = "start_date")
    private LocalDateTime  startDate;
    @Column(name = "end_date")

    private LocalDateTime endDate;
    private String description;
    private String Location;
}
