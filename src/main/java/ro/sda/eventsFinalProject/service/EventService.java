package ro.sda.eventsFinalProject.service;

import org.springframework.stereotype.Service;
import ro.sda.eventsFinalProject.model.Event;
import ro.sda.eventsFinalProject.repository.EventRepository;

import java.util.ArrayList;
import java.util.List;

@Service

public class EventService {
    private final EventRepository eventRepository;

    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    // functioneaza
    public Event saveEvent(Event event) {
        if (event.getName() == null) {
            // Avoid  the genertion of NullPointerException null dates!
            throw new IllegalArgumentException("An event must habe a name!");
        }
        // null.isAfter
        if (event.getStartDate() == null || event.getEndDate() == null
                || event.getStartDate().isAfter(event.getEndDate())) {
            throw new IllegalArgumentException("Event start date is after end date. Please be carefoul!");
        }

        Event savedEvent = eventRepository.save(event);
        return savedEvent;
    }
    public Event readEvent(Integer id){
        if( id == null){
            throw new IllegalArgumentException("EventID must not be null");
        }
        Event event = eventRepository.findById(id).orElse(null);
        if(event == null){
            throw new IllegalArgumentException("There is not event with ID " + id);
        }
        return event;
    }
    public List<Event> readAllEvents(){
        return eventRepository.findAll();
    }
}



