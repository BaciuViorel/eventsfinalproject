package ro.sda.eventsFinalProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventsFinalProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(EventsFinalProjectApplication.class, args);
	}
}
